﻿using Pacman.GameLogic;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Pacman.HelperClasses
{
    class HelperClass
    {
        public static void CreateCells(Game game,Panel panel)
        {
            int y = 0, x = 0;
            for (int i = 0; i < game.Cells.GetLength(0); i++)
            {
                for (int j = 0; j < game.Cells.GetLength(1); j++)
                {
                    Button b = new Button();
                    b.Tag = $"{i}-{j}";
                    b.Size = new System.Drawing.Size(25, 25);
                    b.Location = new System.Drawing.Point(x, y);
                    panel.Controls.Add(b);
                    x += 30;
                }
                y += 30;
                x = 0;
            }
        }
        public  static void RefreshCells(Game game, Panel panel)
        {
            if (game == null)
                return;
            if (game.Cells == null)
                return;
          foreach(Control c in panel.Controls)
            {
                if (c is Button)
                {
                    string[] a = ((Button)c).Tag.ToString().Split('-');

                    ChangeButtonImage(game.Cells[Convert.ToInt32(a[0]), Convert.ToInt32(a[1])].State, (Button)c);
                }
            }
        }
        public static void ChangeButtonImage(CellState state,Button b)
        {
              b.BackgroundImage = null;

            if (state == CellState.Pacman)
                b.BackgroundImage = Image.FromFile("1.png");

            if (state == CellState.Block)
                b.BackgroundImage = Image.FromFile("2.png");

            if (state == CellState.Point)
                b.BackgroundImage = Image.FromFile("3.png");

            b.BackgroundImageLayout = ImageLayout.Stretch;
        }


    }
}
