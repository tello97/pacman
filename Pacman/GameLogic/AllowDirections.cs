﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pacman.GameLogic
{
   public enum Direction {
        Top,
        Bottom,
        Left,
        Right
    }
   public class AllowDirections
    {
        public bool AllowTopMove { get; set; }

        public bool AllowBottomMove { get; set; }

        public bool AllowRightMove { get; set; }

        public bool AllowLeftMove { get; set; }
    }
}
