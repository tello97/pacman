﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pacman.GameLogic
{
  public  enum CellState {
        Pacman,
        Block,
        Point,
        Blank
    }
   public class Cell
    {
        public CellState State { get; set; }
    }
}
