﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Pacman.GameLogic
{
    public class Game:IComparable
    {
        public Cell[,] Cells;
        public Point PacmanLocation=new Point(-1,-1);
        public Game Parent;
        public int BlockCount, PointCount, MovmentCount = 0, TakenPointCount = 0;
        public int Cost { get {return MovmentCount - TakenPointCount; } }
        public Game()
        {

        }
        public Game(int m, int n, int BlockCount, int PointCount)
        {
            Cells = new Cell[m, n];
            this.BlockCount = BlockCount;
            this.PointCount = PointCount;
            this.Parent = new Game();
            for (int i = 0; i < m; i++)
            {
                for (int j = 0; j < n; j++)
                {
                    Cells[i, j] = new Cell();
                    Cells[i, j].State = CellState.Blank;
                }
            }
        }

        public bool SetCellState(int m, int n, CellState State)
        {
            if (!AllowSetCellState(State))
                return false;
            Cells[m, n].State = State;
            if (State == CellState.Pacman)
                PacmanLocation = new Point(m, n);
            return true;
        }
        private bool AllowSetCellState(CellState State)
        {
            if (State == CellState.Pacman)
                return PacmanLocation.X == -1 && PacmanLocation.Y == -1;

            if (State == CellState.Block)
                return Cells.Cast<Cell>().ToList().Where(x => x.State == CellState.Block).Count() < BlockCount;

            if (State == CellState.Point)
                return Cells.Cast<Cell>().ToList().Where(x => x.State == CellState.Point).Count() < PointCount;

            return false;
        }
        public bool CanStartGame()
        {
            return (PacmanLocation.X != -1 || PacmanLocation.Y != -1) && (Cells.Cast<Cell>().ToList().Where(x => x.State == CellState.Block).Count() == BlockCount) && (Cells.Cast<Cell>().ToList().Where(x => x.State == CellState.Point).Count() == PointCount);
        }

        public AllowDirections GetAllowDirections()
        {
            int m = PacmanLocation.X, n = PacmanLocation.Y;
            AllowDirections AD = new AllowDirections();
            try
            {
                AD.AllowTopMove = Cells[m-1, n ].State != CellState.Block;
            }
            catch { AD.AllowTopMove = false; }

            try
            {
                AD.AllowBottomMove = Cells[m+1, n ].State != CellState.Block;
            }
            catch { AD.AllowBottomMove = false; }

            try
            {
                AD.AllowLeftMove = Cells[m, n-1 ].State != CellState.Block;
            }
            catch { AD.AllowLeftMove = false; }

            try
            {
                AD.AllowRightMove = Cells[m, n+1 ].State != CellState.Block;
            }
            catch { AD.AllowRightMove = false; }

            return AD;
        }

        public Game Move(Direction direction)
        {
            var a = JsonConvert.DeserializeObject<Game>(JsonConvert.SerializeObject(this));
            a.Parent = this;
            try
            {
                switch (direction)
                {
                    case Direction.Top:
                        a.TakenPointCount += (a.Cells[a.PacmanLocation.X - 1, a.PacmanLocation.Y].State == CellState.Point) ? 1 : 0;
                        a.Cells[a.PacmanLocation.X - 1, a.PacmanLocation.Y].State = CellState.Pacman;
                        a.Cells[a.PacmanLocation.X, a.PacmanLocation.Y].State = CellState.Blank;
                        a.PacmanLocation = new Point(a.PacmanLocation.X - 1, a.PacmanLocation.Y);
                        break;

                    case Direction.Bottom:
                        a.TakenPointCount += (a.Cells[a.PacmanLocation.X + 1, a.PacmanLocation.Y].State == CellState.Point) ? 1 : 0;
                        a.Cells[a.PacmanLocation.X + 1, a.PacmanLocation.Y].State = CellState.Pacman;
                        a.Cells[a.PacmanLocation.X, a.PacmanLocation.Y].State = CellState.Blank;
                        a.PacmanLocation = new Point(a.PacmanLocation.X + 1, a.PacmanLocation.Y);
                        break;

                    case Direction.Left:
                        a.TakenPointCount += (a.Cells[a.PacmanLocation.X, a.PacmanLocation.Y - 1].State == CellState.Point) ? 1 : 0;
                        a.Cells[a.PacmanLocation.X, a.PacmanLocation.Y - 1].State = CellState.Pacman;
                        a.Cells[a.PacmanLocation.X, a.PacmanLocation.Y].State = CellState.Blank;
                        a.PacmanLocation = new Point(a.PacmanLocation.X, a.PacmanLocation.Y - 1);
                        break;

                    case Direction.Right:
                        a.TakenPointCount += (a.Cells[a.PacmanLocation.X, a.PacmanLocation.Y + 1].State == CellState.Point) ? 1 : 0;
                        a.Cells[a.PacmanLocation.X, a.PacmanLocation.Y + 1].State = CellState.Pacman;
                        a.Cells[a.PacmanLocation.X, a.PacmanLocation.Y].State = CellState.Blank;
                        a.PacmanLocation = new Point(a.PacmanLocation.X, a.PacmanLocation.Y + 1);
                        break;
                }
                a.MovmentCount++;
                return a;
            }
            catch { return null; }
        }

        public List<Game> GetChildes()
        {
            List<Game> Childes = new List<Game>();
            AllowDirections directions = GetAllowDirections();
            if (directions.AllowBottomMove)
                Childes.Add(Move(Direction.Bottom));
            if (directions.AllowLeftMove)
                Childes.Add(Move(Direction.Left));
            if (directions.AllowRightMove)
                Childes.Add(Move(Direction.Right));
            if (directions.AllowTopMove)
                Childes.Add(Move(Direction.Top));
            return Childes;
        }
        public bool IsFinish()
        {
            return PointCount == TakenPointCount;
        }
        public override bool Equals(object obj)
        {
            if (obj == null)
                return false;
            if (obj is Game)
            {
                try
                {
                    for (int i = 0; i < Cells.GetLength(0); i++)
                    {
                        for (int j = 0; j < Cells.GetLength(1); j++)
                        {
                            try
                            {
                                if (this.Cells[i, j].State != ((Game)obj).Cells[i, j].State)
                                    return false;
                            }
                            catch { return false; }
                        }
                    }
                }
                catch { return false; }
                return true;
            }
            return base.Equals(obj);
        }

        public int CompareTo(object obj)
        {
            if (this.MovmentCount > ((Game)obj).MovmentCount)
                return -1;

            if (this.MovmentCount == ((Game)obj).MovmentCount)
                return 0;

            if (this.MovmentCount < ((Game)obj).MovmentCount)
                return +1;

            return 0;

        }
        
        
    }
}
