﻿using Pacman.GameLogic;
using Pacman.HelperClasses;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Pacman.Views
{
    public partial class InitialGameForm : Form
    {
        Game game;
        CellState SelectedCellState;
        public InitialGameForm(Game game)
        {
            this.game = game;
            InitializeComponent();
            CreateCells(game, panel1);
        }
        public void CreateCells(Game game, Panel panel)
        {
            int y = 0, x = 0;
            for (int i = 0; i < game.Cells.GetLength(0); i++)
            {
                for (int j = 0; j < game.Cells.GetLength(1); j++)
                {
                    Button b = new Button();
                    b.Tag = $"{i}-{j}";
                    b.Size = new System.Drawing.Size(25, 25);
                    b.Location = new System.Drawing.Point(x, y);
                    b.Click += B_Click;
                    panel.Controls.Add(b);
                    x += 30;
                }
                y += 30;
                x = 0;
            }
        }

        private void B_Click(object sender, EventArgs e)
        {
            string[] a = ((Button)sender).Tag.ToString().Split('-');
            if (game.SetCellState(Convert.ToInt32(a[0]), Convert.ToInt32(a[1]), SelectedCellState) == false)
            {
                MessageBox.Show("you can't do that");
                return;
            }
            if (SelectedCellState==CellState.Pacman)
            ((Button)sender).BackgroundImage = Image.FromFile("1.png");

            if (SelectedCellState == CellState.Block)
                ((Button)sender).BackgroundImage = Image.FromFile("2.png");

            if (SelectedCellState == CellState.Point)
                ((Button)sender).BackgroundImage = Image.FromFile("3.png");

            ((Button)sender).BackgroundImageLayout = ImageLayout.Stretch;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            SelectedCellState = CellState.Pacman;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            SelectedCellState = CellState.Block;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            SelectedCellState = CellState.Point;
        }

        private void button4_Click(object sender, EventArgs e)
        {
            if (!game.CanStartGame())
            {
                MessageBox.Show("you can't start game");
                return;
            }
            GameForm f = new GameForm(game);
            f.Show();
            this.Hide();
        }
    }
}
