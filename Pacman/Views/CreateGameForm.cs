﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Pacman.Views
{
    public partial class CreateGameForm : Form
    {
        public CreateGameForm()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            InitialGameForm f = new InitialGameForm(new GameLogic.Game(Convert.ToInt32(cc_txt.Text), Convert.ToInt32(rc_txt.Text), Convert.ToInt32(bc_txt.Text), Convert.ToInt32(pc_txt.Text)));
            f.Show();
            this.Hide();
        }
    }
}
