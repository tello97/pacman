﻿using Pacman.Algo;
using Pacman.GameLogic;
using Pacman.HelperClasses;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Pacman.Views
{
    public partial class GameForm : Form
    {
        List<Game> games=new List<Game>();
        List<Game> DFSList = new List<Game>();
        List<Game> BFSList = new List<Game>();
        List<Game> UniformCostList = new List<Game>();
        List<Game> AStar = new List<Game>();

        public GameForm(Game game)
        {
            this.games.Add(game);
            InitializeComponent();
            CreateCells(games.Last(), panel1);
            RefreshCells(games.Last(), panel1);
            DirectionButtonsVisible();
        }


        public static void CreateCells(Game game, Panel panel)
        {
            int y = 0, x = 0;
            for (int i = 0; i < game.Cells.GetLength(0); i++)
            {
                for (int j = 0; j < game.Cells.GetLength(1); j++)
                {
                    Button b = new Button();
                    b.Tag = $"{i}-{j}";
                    b.Size = new System.Drawing.Size(25, 25);
                    b.Location = new System.Drawing.Point(x, y);
                    panel.Controls.Add(b);
                    x += 30;
                }
                y += 30;
                x = 0;
            }
        }
        public void RefreshCells(Game game, Panel panel)
        {
            if (game == null)
                return;
            if (game.Cells == null)
                return;
            foreach (Control c in panel.Controls)
            {
                if (c is Button)
                {
                    string[] a = ((Button)c).Tag.ToString().Split('-');

                    ChangeButtonImage(game.Cells[Convert.ToInt32(a[0]), Convert.ToInt32(a[1])].State, (Button)c);
                }
            }
        }
        public void ChangeButtonImage(CellState state, Button b)
        {
            try
            {
                    b.BackgroundImage = null;
                if (state == CellState.Pacman)
                        b.BackgroundImage = Image.FromFile("1.png");
                if (state == CellState.Block)
                        b.BackgroundImage = Image.FromFile("2.png");
                if (state == CellState.Point)
                        b.BackgroundImage = Image.FromFile("3.png");
                    b.BackgroundImageLayout = ImageLayout.Stretch;
            }
            catch { }
        }

        private void DirectionButtonsVisible()
        {
            AllowDirections AD = games.Last().GetAllowDirections();
            up_btn.Visible = AD.AllowTopMove;
            bottom_btn.Visible = AD.AllowBottomMove;
            left_btn.Visible = AD.AllowLeftMove;
            right_btn.Visible = AD.AllowRightMove;
        }

        private void up_btn_Click(object sender, EventArgs e)
        {
            games.Add(games.Last().Move(Direction.Top));
            RefreshCells(games.Last(), panel1);
            DirectionButtonsVisible();
        }

        private void right_btn_Click(object sender, EventArgs e)
        {
            games.Add(games.Last().Move(Direction.Right));
            RefreshCells(games.Last(), panel1);
            DirectionButtonsVisible();
        }

        private void left_btn_Click(object sender, EventArgs e)
        {
            games.Add(games.Last().Move(Direction.Left));
            RefreshCells(games.Last(), panel1);
            DirectionButtonsVisible();
        }

        private void bottom_btn_Click(object sender, EventArgs e)
        {
            games.Add(games.Last().Move(Direction.Bottom));
            RefreshCells(games.Last(), panel1);
            DirectionButtonsVisible();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                var result = AlgoClass.DFS(games[0]);
                DFSList.Add(result);
                while (result.Parent != null)
                {
                    DFSList.Add(result.Parent);
                    result = result.Parent;
                }
            }
            catch { }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
                var result = AlgoClass.BFS(games[0]);
                BFSList.Add(result);
                while (result.Parent != null)
                {
                    BFSList.Add(result.Parent);
                    result = result.Parent;
                }
            }
            catch { }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            try
            {
                var result = AlgoClass.UbiFormCost(games[0]);
                UniformCostList.Add(result);
                while (result.Parent != null)
                {
                    UniformCostList.Add(result.Parent);
                    result = result.Parent;
                }
            }
            catch { }
        }

        private void numericUpDown3_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                RefreshCells(UniformCostList[UniformCostList.Count - Convert.ToInt32(numericUpDown3.Value)], panel1);
                DirectionButtonsVisible();
            }
            catch { }
        }

        private void numericUpDown2_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                RefreshCells(BFSList[BFSList.Count - Convert.ToInt32(numericUpDown2.Value)], panel1);
                DirectionButtonsVisible();
            }
            catch { }
        }

        private void numericUpDown1_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                RefreshCells(DFSList[DFSList.Count - Convert.ToInt32(numericUpDown1.Value)], panel1);
                DirectionButtonsVisible();
            }
            catch { }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            try
            {
                var result = AlgoClass.AStar(games[0]);
                AStar.Add(result);
                while (result.Parent != null)
                {
                    AStar.Add(result.Parent);
                    result = result.Parent;
                }
            }
            catch { }
        }

        private void numericUpDown4_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                RefreshCells(AStar[AStar.Count - Convert.ToInt32(numericUpDown4.Value)], panel1);
                DirectionButtonsVisible();
            }
            catch { }
        }
    }
}
