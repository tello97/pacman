﻿using Pacman.GameLogic;
using System.Collections.Generic;
using System.Linq;

namespace Pacman.Algo
{
    public class AlgoClass
    {
        //test gitlab
        public static Game DFS(Game root)
        {
            List<Game> visited = new List<Game>();
            Stack<Game> s = new Stack<Game>();
            Game state=new Game();
            s.Push(root);
            while(s.Count()!=0)
            {
                 state = s.Peek();
                if (state.IsFinish())
                    return state;
                if (!visited.Contains(state))
                {
                    visited.Add(state);
                    s.Pop();
                    var childes = state.GetChildes();
                    foreach(var childe in childes)
                    {
                        s.Push(childe);
                    }
                }
                else s.Pop();
            }
            return null;
        }

        public static Game BFS(Game root)
        {
            List<Game> visited = new List<Game>();
            Queue<Game> s = new Queue<Game>();
            Game state = new Game();
            s.Enqueue(root);
            while (s.Count() != 0)
            {
                state = s.Peek();
                if (state.IsFinish())
                    return state;
                if (!visited.Contains(state))
                {
                    visited.Add(state);

                    s.Dequeue();
                    var childes = state.GetChildes();
                    foreach (var childe in childes)
                    {
                        s.Enqueue(childe);
                    }
                }
                else s.Dequeue();
            }
            return null;
        }

        public static Game UbiFormCost(Game root)
        {
            List<Game> visited = new List<Game>();
            List<Game> s = new List<Game>();
            Game state = new Game();
            s.Add(root);
            while (s.Count() != 0)
            {
                s.Sort();
                state = s.First();
                if (state.IsFinish())
                    return state;
                if (!visited.Contains(state))
                {
                    visited.Add(state);
                    s.RemoveAt(s.IndexOf(s.First()));
                    var childes = state.GetChildes();
                    foreach (var childe in childes)
                    {
                        s.Add(childe);
                    }
                }
                else s.RemoveAt(s.IndexOf(s.First()));
            }
            return null;
        }
        public static Game AStar(Game root)
        {
            List<Game> visited = new List<Game>();
            List<Game> s = new List<Game>();
            Game state = new Game();
            s.Add(root);
            while (s.Count() != 0)
            {
                s.OrderBy(x=>x.Cost);
                state = s.First();
                if (state.IsFinish())
                    return state;
                if (CheckVisited(state,visited)==true)
                {
                    s.RemoveAt(s.IndexOf(s.First()));
                }
                else if (CheckVisited(state, visited) == false)
                {
                    visited.Add(state);
                    s.RemoveAt(s.IndexOf(s.First()));
                    var childes = state.GetChildes();
                    foreach (var childe in childes)
                    {
                            s.Add(childe);
                    }
                }
                else if (CheckVisited(state, visited) == false)
                {
                    whencostless(state, visited);
                }
            }
            return null;
        }
        private static bool? CheckVisited(Game node,List<Game> visited)
        {
            if (visited.Contains(node))
            {
                foreach (var x in visited)
                    if (x.Equals(node))
                        if (x.Cost > node.Cost)
                            return null;
                return true;

            }
            return false;
        }
        private static void whencostless(Game node, List<Game> visited)
        {
            foreach (var x in visited)
                if (x.Equals(node))
                    visited.Remove(x);
            visited.Add(node);
        }
    }
}
