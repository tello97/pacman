﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pacman.GraphDataStructure
{
   public class Graph
    {
        adjList[] array;
        public Graph(int size)
        {
            array = new adjList[size];
            for (int i = 0; i < size; i++)
            {
                array[i] = new adjList();
                array[i].head = null; 
            }
        }
    }
}
