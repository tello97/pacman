﻿using Pacman.GameLogic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pacman.GraphDataStructure
{
   public class Node
    {
        Game Data;
        Node Next;
        public Node(Game data,Node next)
        {
            this.Data = data;
            this.Next = next;
        }
    }
}
